from backgammon import backgammon
import random

for i in range(100):
    if(i%10 == 0):
        # Instantiate the backgammon object and save board state for a static start
        game = backgammon()
        static_start = game.get_board()
    else:
        # Instantiate the backgammon object with the saved board static start
        game = backgammon(static_start)

    # Iterate over the moves until a winner is determined

    print(game.get_board())

    while game.get_winner() is None:
        # Iterate over the moves directly from the moves attribute

        #Get who's turn it is
        board = game.get_board()
        RED = 0
        if(board[-1]==1):
            RED = 1

        #Swap agents based on who's turn it currently is
        if(RED):
            agent = random.random()
        else:
            agent = random.random()

        #Iterate through the moves list at the current board state and create scalar values
        for move in game.moves:
            # Randomly generate a scalar value
            score = agent

            # Call the store_move function to store the move and scalar value
            game.score_move(move, score)

        # Check if a winner is determined after each iteration
        if game.get_winner() is not None:
            break